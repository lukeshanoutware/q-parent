# q_parents

A new Flutter application.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

### Upgrade Flutter
Run `flutter upgrade` in the root directory

### Get/Upgrade dependencies
Run `flutter packages get` or `flutter packages upgrade` in the root directory

### Generate files into source directory.
Run `flutter packages pub run build_runner build --delete-conflicting-outputs` in the root directory

### Generate localization files into source directory.
Run `flutter pub run intl_translation:extract_to_arb --output-dir=lib/localizations lib/localizations/q_parent_localizations.dart` in the root directory
Run `flutter pub run intl_translation:generate_from_arb --output-dir=lib/localizations --no-use-deferred-loading lib/localizations/q_parent_localizations.dart lib/localizations/intl_*.arb` in the root directory
