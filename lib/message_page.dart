import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:q_parents/account_setup_page.dart';
import 'package:q_parents/bloc/message_panel/message_panel_bloc.dart';
import 'package:q_parents/bloc/message_panel/message_panel_state.dart';
import 'package:q_parents/localizations/q_parent_localizations.dart';
import 'package:q_parents/widget/message_panel.dart';

class MessageRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MessagePage();
  }
}

class MessagePage extends StatefulWidget {
  @override
  _MessagePageState createState() {
    return _MessagePageState();
  }
}

class _MessagePageState extends State<MessagePage> {
  final MessagePanelBLoC _bLoC = MessagePanelBLoC(MessagePanelInitialState());

  _MessagePageState();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Locale myLocale = Localizations.localeOf(context);
    print("_MessagePageState $myLocale");
    return Scaffold(
        appBar: AppBar(
          brightness: Brightness.dark,
          leading: Padding(
            padding: const EdgeInsets.all(8.0),
            child: SvgPicture.asset(
              "assets/images/parents.svg",
              alignment: Alignment.bottomCenter,
              fit: BoxFit.contain,
              color: Colors.white,
            ),
          ),
          title: Text(Gjh.of(context).d("message")),
          actions: <Widget>[
            Builder(builder: (BuildContext context) {
              return IconButton(
                tooltip: MaterialLocalizations
                    .of(context)
                    .showMenuTooltip,
                icon: Icon(Icons.settings),
                onPressed: () => _setupAccount(context),
              );
            })
          ],
        ),
        body: MessagePanel(_bLoC));
  }

  void _setupAccount(BuildContext context) async {
    await Navigator.of(context).push(MaterialPageRoute(builder: (context) => AccountSetupRoute()));
    _bLoC.initialise();
  }
}
