import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:q_parents/localizations/q_parent_localizations.dart';
import 'package:q_parents/widget/account_form.dart';

class AccountSetupRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AccountSetupPage();
  }
}

class AccountSetupPage extends StatefulWidget {
  @override
  _AccountSetupPageState createState() {
    return _AccountSetupPageState();
  }
}

class _AccountSetupPageState extends State<AccountSetupPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          brightness: Brightness.dark,
          leading: Padding(
            padding: const EdgeInsets.all(8.0),
            child: SvgPicture.asset(
              "assets/images/parents.svg",
              alignment: Alignment.bottomCenter,
              fit: BoxFit.contain,
              color: Colors.white,
            ),
          ),
          title: Text(Gjh.of(context).d("accountSetup"))),
      body: AccountForm(),
    );
  }
}
