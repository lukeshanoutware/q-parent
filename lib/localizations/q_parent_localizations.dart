import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:q_parents/localizations/messages_all.dart';

class Gjh {
  final Locale _locale;

  Gjh(this._locale);

  static Gjh of(BuildContext context) {
    return Localizations.of<Gjh>(context, Gjh);
  }

  static Map<String, Map<String, String>> _localizedValues = {
    'message': {'en': 'Message', 'zh': '消息'},
    'noMessage': {'en': 'You have no message', 'zh': '您没有消息'},
    'save': {'en': 'Save', 'zh': '保存'},
    'initialising': {'en': 'Initialising...', 'zh': '初始化中……'},
    'setupAccountLabel': {'en': 'Please setup your account first', 'zh': '请先设置您的帐户'},
    'setupAccountButton': {'en': 'Setup Account', 'zh': '设置帐户'},
    'accountSetup': {'en': 'Account Setup', 'zh': '帐户设置'},
    'parentNameHint': {'en': 'Please enter parent name here', 'zh': '请输入家长名字'},
    'studentNameHint': {'en': 'Please enter student name here', 'zh': '请输入学生名字'},
    'accountInfoSaved': {'en': 'Account info saved', 'zh': '帐户信息已被保存'},
    'acknowledge': {'en': 'Acknowledge', 'zh': '认可'},
    'messageAcknowledged': {'en': 'Message acknowledged', 'zh': '消息已被认可'},
    'qParent': {'en': 'Q Parent', 'zh': '昆州家长'},
  };

  String d(String resourceName) {
    return _localizedValues[resourceName][_locale.languageCode];
  }
}

class GjhDelegate extends LocalizationsDelegate<Gjh> {
  const GjhDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'zh'].contains(locale.languageCode);

  @override
  Future<Gjh> load(Locale locale) {
    return SynchronousFuture<Gjh>(Gjh(locale));
  }

  @override
  bool shouldReload(GjhDelegate old) => false;
}

class QParentLocalizations {
  QParentLocalizations(this._locale);

  static Future<QParentLocalizations> load(Locale locale) {
    final String name = (locale.countryCode == null || locale.countryCode.isEmpty) ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);

    return initializeMessages(localeName).then((_) {
      return QParentLocalizations(localeName);
    });
  }

  static QParentLocalizations of(BuildContext context) {
    return Localizations.of<QParentLocalizations>(context, QParentLocalizations);
  }

  final String _locale;

  String get save {
    return Intl.message(
      'Save',
      name: 'save',
      desc: 'String for save action',
      locale: _locale,
    );
  }

  String get parentNameHint {
    return Intl.message(
      'Please enter parent name here',
      name: 'parentNameHint',
      desc: 'String for parent name field hint',
      locale: _locale,
    );
  }
}

class QParentLocalizationsDelegate extends LocalizationsDelegate<QParentLocalizations> {
  const QParentLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'zh'].contains(locale.languageCode);

  @override
  Future<QParentLocalizations> load(Locale locale) => QParentLocalizations.load(locale);

  @override
  bool shouldReload(QParentLocalizationsDelegate old) => false;
}
