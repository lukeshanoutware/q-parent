import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:q_parents/localizations/q_parent_localizations.dart';
import 'package:q_parents/message_page.dart';

void main() {
  debugPaintSizeEnabled = false;
  runApp(QParentsApp());
}

class QParentsApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        const GjhDelegate(),
        const QParentLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en', 'US'), // English
        const Locale('zh'), // Chinese
      ],
      onGenerateTitle: (context) => Gjh.of(context).d("qParent"),
      theme: ThemeData(primarySwatch: Colors.green, accentColor: Colors.orangeAccent),
      home: MessagePage(),
    );
  }
}
