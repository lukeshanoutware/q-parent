class ApplicationModel {
  static const String API_URL = "https://us-central1-call-the-roll-945ca.cloudfunctions.net/helloWorld";

  static final ApplicationModel instance = new ApplicationModel._internal();

  factory ApplicationModel() => instance;

  ApplicationModel._internal() {}
}
