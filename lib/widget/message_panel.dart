import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:q_parents/account_setup_page.dart';
import 'package:q_parents/bloc/message_panel/message_panel_bloc.dart';
import 'package:q_parents/bloc/message_panel/message_panel_state.dart';
import 'package:q_parents/localizations/q_parent_localizations.dart';

class MessagePanel extends StatefulWidget {
  final MessagePanelBLoC _bLoC;

  MessagePanel(this._bLoC);

  @override
  _MessagePanelState createState() {
    return _MessagePanelState(_bLoC);
  }
}

class _MessagePanelState extends State<MessagePanel> with WidgetsBindingObserver {
  final MessagePanelBLoC _bLoC;
  Gjh gjh;

  _MessagePanelState(this._bLoC);

  @override
  void initState() {
    print("initState in _MessagePanelState");
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    _bLoC.initialise();
  }

  @override
  void dispose() {
    print("dispose in _MessagePanelState");
    _bLoC.dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didUpdateWidget(MessagePanel oldWidget) {
    print("didUpdateWidget in _MessagePanelState");
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print("didChangeAppLifecycleState $state in _MessagePanelState");
    if (state == AppLifecycleState.resumed) {
      print("AppLifecycleState.resumed in _MessagePanelState");
      _bLoC.initialise();
    }
  }

  @override
  Widget build(BuildContext context) {
    print("build in _MessagePanelState");
    gjh = Gjh.of(context);
    return StreamBuilder<MessagePanelState>(
      stream: _bLoC.stream,
      initialData: _bLoC.initialData,
      builder: (BuildContext context, AsyncSnapshot<MessagePanelState> snapshot) {
        final state = snapshot.data;
        if (state is MessagePanelAcknowledgedState) {
          WidgetsBinding.instance.addPostFrameCallback((_) => _showSnackBar());
          return _noMessage();
        } else if (state is MessagePanelIdleState) {
          return _noMessage();
        } else if (state is MessagePanelReceivedState) {
          return _messageDetails(state.title, state.message, state.time);
        } else if (state is MessagePanelAcknowledgingState) {
          return _messageDetails(state.title, state.message, state.time, enabled: false);
        } else if (state is MessagePanelInitialState) {
          return _initialising();
        } else {
          return _needAccountSetup();
        }
      },
    );
  }

  Widget _needAccountSetup() {
    return Center(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              gjh.d("setupAccountLabel"),
              style: Theme
                  .of(context)
                  .textTheme
                  .bodyText1,
            ),
          ),
          _setupAccountButton(),
        ],
      ),
    );
  }

  Widget _initialising() {
    return Center(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              gjh.d('initialising'),
              style: Theme
                  .of(context)
                  .textTheme
                  .bodyText2,
            ),
          ),
        ],
      ),
    );
  }

  Widget _noMessage() {
    return Center(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              gjh.d("noMessage"),
              style: Theme
                  .of(context)
                  .textTheme
                  .bodyText2,
            ),
          ),
        ],
      ),
    );
  }

  Widget _messageDetails(String title, String message, String time, {bool enabled = true}) {
    return Center(
      child: Column(
        children: <Widget>[_title(title), _message(message), _time(time), _acknowledgeButton(enabled: enabled)],
      ),
    );
  }

  Widget _title(String title) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        title,
        style: Theme
            .of(context)
            .textTheme
            .headline6,
      ),
    );
  }

  Widget _message(String message) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        message,
        style: Theme
            .of(context)
            .textTheme
            .bodyText1,
      ),
    );
  }

  Widget _time(String time) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        time,
        style: Theme
            .of(context)
            .textTheme
            .caption,
      ),
    );
  }

  Widget _acknowledgeButton({bool enabled = true}) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: RaisedButton(
        child: Text(gjh.d("acknowledge")),
        onPressed: _onAcknowledgeButtonPressed(enabled),
      ),
    );
  }

  Widget _setupAccountButton() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: RaisedButton(
        child: Text(gjh.d('setupAccountButton')),
        onPressed: () => _navigateToAccountSetup(),
      ),
    );
  }

  Function _onAcknowledgeButtonPressed(bool enabled) {
    if (enabled) {
      return () {
        _acknowledge();
      };
    } else {
      return null;
    }
  }

  void _navigateToAccountSetup() async {
    await Navigator.of(context).push(MaterialPageRoute(builder: (context) => AccountSetupRoute()));
    print("_navigateToAccountSetup popped");
    _bLoC.initialise();
  }

  void _acknowledge() {
    _bLoC.acknowledge();
  }

  void _showSnackBar() {
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text(gjh.d("messageAcknowledged")),
    ));
  }
}
