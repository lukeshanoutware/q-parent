import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:q_parents/bloc/account_form/account_form_bloc.dart';
import 'package:q_parents/bloc/account_form/account_form_state.dart';
import 'package:q_parents/localizations/q_parent_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AccountForm extends StatefulWidget {
  @override
  _AccountFormState createState() {
    return _AccountFormState();
  }
}

class _AccountFormState extends State<AccountForm> {
  final AccountFormBLoC _bLoC = AccountFormBLoC(AccountFormIdleState());
  final _parentNameController = TextEditingController();
  final _studentNameController = TextEditingController();
  Future<SharedPreferences> _sharedPreferences = SharedPreferences.getInstance();
  Gjh gjh;

  _AccountFormState();

  @override
  void dispose() {
    _parentNameController.dispose();
    _studentNameController.dispose();
    _bLoC.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    gjh = Gjh.of(context);
    return StreamBuilder<AccountFormState>(
      stream: _bLoC.stream,
      initialData: _bLoC.initialData,
      builder: (BuildContext context, AsyncSnapshot<AccountFormState> snapshot) {
        if (snapshot.data is AccountFormIdleState || snapshot.data is AccountFormSavedState || snapshot.data is AccountFormNavigateBackState) {
          if (snapshot.data is AccountFormSavedState) {
            WidgetsBinding.instance.addPostFrameCallback((_) => _showSnackBar());
            _bLoC.navigateBack();
          }
          return Column(
            children: <Widget>[_parentNameField(), _studentNameField(), _saveButton()],
          );
        } else {
          return Column(
            children: <Widget>[_parentNameField(enabled: false), _studentNameField(enabled: false), _saveButton(enabled: false)],
          );
        }
      },
    );
  }

  Widget _parentNameField({bool enabled = true}) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: FutureBuilder<String>(
        future: _sharedPreferences.then(
              (SharedPreferences sharedPreferences) {
            return (sharedPreferences.getString('name') ?? "");
          },
        ),
        builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
          _parentNameController.text = snapshot.data;
          return TextFormField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: gjh.d("parentNameHint"),
            ),
            controller: _parentNameController,
            enabled: enabled,
          );
        },
      ),
    );
  }

  Widget _studentNameField({bool enabled = true}) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: FutureBuilder<String>(future: _sharedPreferences.then((SharedPreferences sharedPreferences) {
        return (sharedPreferences.getString('student_name') ?? "");
      }), builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
        _studentNameController.text = snapshot.data;
        return TextFormField(
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: gjh.d("studentNameHint"),
          ),
          controller: _studentNameController,
          enabled: enabled,
        );
      }),
    );
  }

  Widget _saveButton({bool enabled = true}) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: RaisedButton(
        child: Text(gjh.d("save")),
        onPressed: _onSaveButtonPressed(enabled),
      ),
    );
  }

  Function _onSaveButtonPressed(bool enabled) {
    if (enabled) {
      return () {
        _saveAccountInfo();
      };
    } else {
      return null;
    }
  }

  void _saveAccountInfo() {
    final parentName = _parentNameController.text;
    final studentName = _studentNameController.text;
    _bLoC.saveAccount(parentName, studentName);
  }

  void _showSnackBar() {
    Scaffold
        .of(context)
        .showSnackBar(SnackBar(
      content: Text(gjh.d("accountInfoSaved")),
    ))
        .closed
        .then((reason) {
      Navigator.of(context).pop();
    });
  }
}
