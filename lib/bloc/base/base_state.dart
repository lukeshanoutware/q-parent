import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class BaseState extends Equatable {}
