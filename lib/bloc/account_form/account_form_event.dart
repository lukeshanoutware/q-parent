import 'package:q_parents/bloc/base/base_event.dart';

abstract class AccountFormEvent extends BaseEvent {}

class AccountFormSaveEvent extends AccountFormEvent {
  final String parentName;
  final String studentName;

  AccountFormSaveEvent(this.parentName, this.studentName);

  @override
  List<Object> get props => [parentName, studentName];
}

class AccountFormWantNavigationBackEvent extends AccountFormEvent {
  AccountFormWantNavigationBackEvent();

  @override
  List<Object> get props => [];
}
