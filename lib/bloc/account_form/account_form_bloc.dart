import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:device_info/device_info.dart';
import 'package:q_parents/bloc/account_form/account_form_event.dart';
import 'package:q_parents/bloc/account_form/account_form_state.dart';
import 'package:q_parents/bloc/base/base_bloc.dart';
import 'package:q_parents/model/parent.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AccountFormBLoC extends BaseBLoC {
  final StreamController<AccountFormEvent> _eventController = StreamController<AccountFormEvent>();
  final StreamController<AccountFormState> _stateController = StreamController<AccountFormState>.broadcast();
  StreamSubscription _eventSubscription;
  AccountFormState _currentState;

  Stream<AccountFormState> get stream => _stateController.stream;

  AccountFormState get initialData => _currentState;

  AccountFormBLoC(AccountFormState initialState) {
    _currentState = initialState;
    _eventSubscription = _eventController.stream.listen((data) => _onEventDataHandler(data), onError: (error, stackTrace) => _onEventErrorHandler(error, stackTrace));
  }

  void _onEventDataHandler(AccountFormEvent event) {
    if (event is AccountFormSaveEvent) {
      _handleSaveEvent(event);
    } else if (event is AccountFormWantNavigationBackEvent) {
      _handleWantNavigationBackEvent(event);
    } else {
      _currentState = AccountFormIdleState();
      _stateController.sink.add(_currentState);
    }
  }

  void _onEventErrorHandler(error, StackTrace stackTrace) {}

  Future<void> _handleSaveEvent(AccountFormSaveEvent event) async {
    final parent = await _createParent(event.parentName, event.studentName);
    _currentState = AccountFormSavingState(parent);
    _stateController.sink.add(_currentState);
    await _saveAccountOnDevice(parent);
    await _saveAccountOnCloud(parent);
    _currentState = AccountFormSavedState(parent);
    _stateController.sink.add(_currentState);
  }

  Future<void> _handleWantNavigationBackEvent(AccountFormWantNavigationBackEvent event) async {
    _currentState = AccountFormNavigateBackState();
    _stateController.sink.add(_currentState);
  }

  @override
  void dispose() {
    _eventSubscription.cancel();
    _eventController.close();
    _stateController.close();
  }

  Future<void> _saveAccountOnDevice(Parent parent) async {
    await SharedPreferences.getInstance().then((SharedPreferences sharedPreferences) {
      sharedPreferences.setString("name", parent.name);
      sharedPreferences.setString("student_name", parent.studentName);
    });
  }

  Future<void> _saveAccountOnCloud(Parent parent) async {
    if (parent.pushToken != "" && parent.name != "" && parent.studentName != "") {
      await Firestore.instance.collection('parents').document(parent.deviceId).get().then((DocumentSnapshot ds) {
        if (ds.exists) {
          final String studentName = ds.data['student_name'];
          Firestore.instance.collection('students').document(studentName.toLowerCase()).collection('parents').document(parent.deviceId).delete();
        }
      });
      await Firestore.instance.collection('students').document(parent.studentName.toLowerCase()).collection('parents').document(parent.deviceId).setData(parent.toJson());
      await Firestore.instance.collection('parents').document(parent.deviceId).setData(parent.toJson());
    }
  }

  Future<Parent> _createParent(String parentName, String studentName) async {
    final sharedPreferences = await SharedPreferences.getInstance();
    final pushToken = sharedPreferences.getString('push_token') ?? "";
    final deviceId = await _getDeviceId();
    return Parent(parentName, studentName, pushToken, deviceId, Platform.operatingSystem);
  }

  Future<String> _getDeviceId() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Platform.isIOS) {
      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
    } else if (Platform.isAndroid) {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      return androidDeviceInfo.androidId; // unique ID on Android
    } else {
      return "fixed_device_id";
    }
  }

  void saveAccount(String parentName, String studentName) {
    _eventController.sink.add(AccountFormSaveEvent(parentName, studentName));
  }

  void navigateBack() {
    _eventController.sink.add(AccountFormWantNavigationBackEvent());
  }
}
