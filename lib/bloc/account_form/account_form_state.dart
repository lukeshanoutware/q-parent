import 'package:q_parents/bloc/base/base_state.dart';
import 'package:q_parents/model/parent.dart';

abstract class AccountFormState extends BaseState {}

class AccountFormIdleState extends AccountFormState {
  AccountFormIdleState();

  @override
  List<Object> get props => [];
}

class AccountFormSavingState extends AccountFormState {
  final Parent parent;

  AccountFormSavingState(this.parent);

  @override
  List<Object> get props => [parent];
}

class AccountFormSavedState extends AccountFormState {
  final Parent parent;

  AccountFormSavedState(this.parent);

  @override
  List<Object> get props => [parent];
}

class AccountFormNavigateBackState extends AccountFormState {
  AccountFormNavigateBackState();

  @override
  List<Object> get props => [];
}
