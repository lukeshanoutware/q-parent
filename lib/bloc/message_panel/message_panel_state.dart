import 'package:q_parents/bloc/base/base_state.dart';

abstract class MessagePanelState extends BaseState {}

class MessagePanelInitialState extends MessagePanelState {
  MessagePanelInitialState();

  @override
  List<Object> get props => [];
}

class MessagePanelNoAccountSetupState extends MessagePanelState {
  MessagePanelNoAccountSetupState();

  @override
  List<Object> get props => [];
}

class MessagePanelIdleState extends MessagePanelState {
  MessagePanelIdleState();

  @override
  List<Object> get props => [];
}

class MessagePanelReceivedState extends MessagePanelState {
  final String title;
  final String message;
  final String time;

  MessagePanelReceivedState(this.title, this.message, this.time);

  @override
  List<Object> get props => [];
}

class MessagePanelAcknowledgingState extends MessagePanelState {
  final String title;
  final String message;
  final String time;

  MessagePanelAcknowledgingState(this.title, this.message, this.time);

  @override
  List<Object> get props => [];
}

class MessagePanelAcknowledgedState extends MessagePanelState {
  MessagePanelAcknowledgedState();

  @override
  List<Object> get props => [];
}
