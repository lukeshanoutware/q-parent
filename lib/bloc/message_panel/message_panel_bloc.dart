import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:intl/intl.dart';
import 'package:q_parents/bloc/base/base_bloc.dart';
import 'package:q_parents/bloc/message_panel/message_panel_event.dart';
import 'package:q_parents/bloc/message_panel/message_panel_state.dart';
import 'package:q_parents/model/parent.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MessagePanelBLoC extends BaseBLoC {
  final StreamController<MessagePanelEvent> _eventController = StreamController<MessagePanelEvent>();
  final StreamController<MessagePanelState> _stateController = StreamController<MessagePanelState>.broadcast();
  StreamSubscription _eventSubscription;
  MessagePanelState _currentState;
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  Stream<MessagePanelState> get stream => _stateController.stream;

  MessagePanelState get initialData => _currentState;

  MessagePanelBLoC(MessagePanelState initialState) {
    _currentState = initialState;
    _eventSubscription = _eventController.stream.listen((data) => _onEventDataHandler(data), onError: (error, stackTrace) => _onEventErrorHandler(error, stackTrace));
  }

  void _onEventDataHandler(MessagePanelEvent event) {
    if (event is MessagePanelInitialiseEvent) {
      _handleInitialiseEvent(event);
    } else if (event is MessagePanelReceivedEvent) {
      _handleReceivedEvent(event);
    } else if (event is MessagePanelAcknowledgeEvent) {
      _handleAcknowledgeEvent(event);
    } else {
      _currentState = MessagePanelIdleState();
      _stateController.sink.add(_currentState);
    }
  }

  void _onEventErrorHandler(error, StackTrace stackTrace) {}

  Future<void> _handleInitialiseEvent(MessagePanelInitialiseEvent event) async {
    _initialiseFirebaseCloudMessaging();
    final sharedPreferences = await SharedPreferences.getInstance();
    final name = sharedPreferences.getString('name') ?? "";
    final studentName = sharedPreferences.getString('student_name') ?? "";
    if (name == "" || studentName == "") {
      _currentState = MessagePanelNoAccountSetupState();
    } else {
      final title = sharedPreferences.getString('title') ?? "";
      final message = sharedPreferences.getString('message') ?? "";
      final time = sharedPreferences.getString('time') ?? "";
      if (title != "" || message != "") {
        _currentState = MessagePanelReceivedState(title, message, time);
      } else {
        _currentState = MessagePanelIdleState();
      }
    }
    _stateController.sink.add(_currentState);
  }

  Future<void> _handleReceivedEvent(MessagePanelReceivedEvent event) async {
    String title = event.message["notification"]["title"];
    String message = event.message["notification"]["body"];
    if (title == null || message == null) {
      title = event.message["data"]["title"];
      message = event.message["data"]["body"];
    }
    if (title != null && message != null) {
      final time = DateFormat("dd/MM/yyyy HH:mm:ss").format(DateTime.now());
      await _saveMessage(title, message, time);
      _currentState = MessagePanelReceivedState(title, message, time);
    } else {
      _currentState = MessagePanelIdleState();
    }
    _stateController.sink.add(_currentState);
  }

  Future<void> _handleAcknowledgeEvent(MessagePanelAcknowledgeEvent event) async {
    final state = (_currentState as MessagePanelReceivedState);
    _currentState = MessagePanelAcknowledgingState(state.title, state.message, state.time);
    _stateController.sink.add(_currentState);
    await _deleteMessage();
    _currentState = MessagePanelAcknowledgedState();
    _stateController.sink.add(_currentState);
  }

  @override
  void dispose() {
    _eventSubscription.cancel();
    _eventController.close();
    _stateController.close();
  }

  Future<String> _getDeviceId() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Platform.isIOS) {
      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
    } else if (Platform.isAndroid) {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      return androidDeviceInfo.androidId; // unique ID on Android
    } else {
      return "fixed_device_id";
    }
  }

  void _initialiseFirebaseCloudMessaging() {
    print("_initialiseFirebaseCloudMessaging");

    if (Platform.isIOS) _iOSPermission();

    _firebaseMessaging.getToken().then((token) {
      _createParent(token).then((Parent parent) {
        _saveAccountOnCloud(parent);
      });
    });

    _firebaseMessaging.configure(
      onMessage: _onMessageHandler,
      onBackgroundMessage: _backgroundMessageHandler,
      onResume: _onResumeHandler,
      onLaunch: _onLaunchHandler,
    );
  }

  void _iOSPermission() {
    _firebaseMessaging.requestNotificationPermissions(IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered.listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  Future<void> _saveAccountOnCloud(Parent parent) async {
    if (parent.pushToken != "" && parent.name != "" && parent.studentName != "") {
      await Firestore.instance.collection('parents').document(parent.deviceId).get().then((DocumentSnapshot ds) {
        if (ds.exists) {
          final String studentName = ds.data['student_name'];
          Firestore.instance.collection('students').document(studentName.toLowerCase()).collection('parents').document(parent.deviceId).delete();
        }
      });
      await Firestore.instance.collection('students').document(parent.studentName.toLowerCase()).collection('parents').document(parent.deviceId).setData(parent.toJson());
      await Firestore.instance.collection('parents').document(parent.deviceId).setData(parent.toJson());
    }
  }

  Future<void> _saveMessage(String title, String message, String time) async {
    final sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setString("title", title);
    await sharedPreferences.setString("message", message);
    await sharedPreferences.setString("time", time);
  }

  Future<void> _deleteMessage() async {
    final sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setString("title", "");
    await sharedPreferences.setString("message", "");
    await sharedPreferences.setString("time", "");
  }

  Future<Parent> _createParent(String pushToken) async {
    final sharedPreferences = await SharedPreferences.getInstance();
    final parentName = sharedPreferences.getString("name") ?? "";
    final studentName = sharedPreferences.getString('student_name') ?? "";
    final deviceId = await _getDeviceId();
    return Parent(parentName, studentName, pushToken, deviceId, Platform.operatingSystem);
  }

  Future<void> _onMessageHandler(Map<String, dynamic> message) async {
    print('on message $message');
    messageReceived(message);
  }

  Future<void> _onResumeHandler(Map<String, dynamic> message) async {
    print('on resume $message');
    messageReceived(message);
  }

  Future<void> _onLaunchHandler(Map<String, dynamic> message) async {
    print('on launch $message');
    messageReceived(message);
  }

  void initialise() {
    _eventController.sink.add(MessagePanelInitialiseEvent());
  }

  void messageReceived(Map<String, dynamic> message) {
    _eventController.sink.add(MessagePanelReceivedEvent(message));
  }

  void acknowledge() {
    _eventController.sink.add(MessagePanelAcknowledgeEvent());
  }
}

Future<dynamic> _backgroundMessageHandler(Map<String, dynamic> message) async {
  print("_backgroundMessageHandler: $message");

  if (message.containsKey('data')) {
    // Handle data message
    final String data = message['data'];
    print("background data is $data");
  }

  if (message.containsKey('notification')) {
    // Handle notification message
    final String notification = message['notification'];
    print("background notification is $notification");
  }

  return Future<void>.value();
}
