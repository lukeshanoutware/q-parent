import 'package:q_parents/bloc/base/base_event.dart';

abstract class MessagePanelEvent extends BaseEvent {}

class MessagePanelInitialiseEvent extends MessagePanelEvent {
  MessagePanelInitialiseEvent();

  @override
  List<Object> get props => [];
}

class MessagePanelReceivedEvent extends MessagePanelEvent {
  final Map<String, dynamic> message;

  MessagePanelReceivedEvent(this.message);

  @override
  List<Object> get props => [message];
}

class MessagePanelAcknowledgeEvent extends MessagePanelEvent {
  MessagePanelAcknowledgeEvent();

  @override
  List<Object> get props => [];
}
