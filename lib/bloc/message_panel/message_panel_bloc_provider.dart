import 'package:flutter/widgets.dart';
import 'package:q_parents/bloc/account_form/account_form_bloc.dart';
import 'package:q_parents/bloc/account_form/account_form_state.dart';

class AccountFormBLoCProvider extends InheritedWidget {
  final AccountFormBLoC bLoC = AccountFormBLoC(AccountFormIdleState());

  AccountFormBLoCProvider({Key key, Widget child}) : super(key: key, child: child);

  @override
  bool updateShouldNotify(_) => true;

  static AccountFormBLoC of(BuildContext context) => (context.inheritFromWidgetOfExactType(AccountFormBLoCProvider) as AccountFormBLoCProvider).bLoC;
}
