import 'package:json_annotation/json_annotation.dart';
import 'package:q_parents/model/student.dart';
import 'package:q_parents/model/teacher.dart';

part 'roll_calling.g.dart';

@JsonSerializable(explicitToJson: true)
class RollCalling {
  @JsonKey(name: 'class_id')
  String classId;
  @JsonKey(name: 'student_list')
  List<Student> studentList;
  @JsonKey(name: 'teacher')
  Teacher teacher;
  @JsonKey(name: 'update_time')
  DateTime updateTime;

  RollCalling(this.classId, this.studentList, this.teacher, this.updateTime);

  factory RollCalling.fromJson(Map<String, dynamic> json) => _$RollCallingFromJson(json);

  Map<String, dynamic> toJson() => _$RollCallingToJson(this);
}
