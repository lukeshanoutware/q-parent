// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'roll_calling.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RollCalling _$RollCallingFromJson(Map<String, dynamic> json) {
  return RollCalling(
    json['class_id'] as String,
    (json['student_list'] as List)
        ?.map((e) =>
    e == null ? null : Student.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['teacher'] == null
        ? null
        : Teacher.fromJson(json['teacher'] as Map<String, dynamic>),
    json['update_time'] == null
        ? null
        : DateTime.parse(json['update_time'] as String),
  );
}

Map<String, dynamic> _$RollCallingToJson(RollCalling instance) =>
    <String, dynamic>{
      'class_id': instance.classId,
      'student_list': instance.studentList?.map((e) => e?.toJson())?.toList(),
      'teacher': instance.teacher?.toJson(),
      'update_time': instance.updateTime?.toIso8601String(),
    };
