// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'parent.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Parent _$ParentFromJson(Map<String, dynamic> json) {
  return Parent(
    json['name'] as String,
    json['student_name'] as String,
    json['push_token'] as String,
    json['device_id'] as String,
    json['device_type'] as String,
  );
}

Map<String, dynamic> _$ParentToJson(Parent instance) => <String, dynamic>{
      'name': instance.name,
      'student_name': instance.studentName,
      'push_token': instance.pushToken,
  'device_id': instance.deviceId,
  'device_type': instance.deviceType,
    };
