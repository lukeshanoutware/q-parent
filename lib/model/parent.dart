import 'package:json_annotation/json_annotation.dart';

part 'parent.g.dart';

@JsonSerializable()
class Parent {
  @JsonKey(name: 'name')
  String name;
  @JsonKey(name: 'student_name')
  String studentName;
  @JsonKey(name: 'push_token')
  String pushToken;
  @JsonKey(name: 'device_id')
  String deviceId;
  @JsonKey(name: 'device_type')
  String deviceType;

  Parent(this.name, this.studentName, this.pushToken, this.deviceId, this.deviceType);

  factory Parent.fromJson(Map<String, dynamic> json) => _$ParentFromJson(json);

  Map<String, dynamic> toJson() => _$ParentToJson(this);
}
